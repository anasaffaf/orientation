#Git Summary  
_Version Control_ systems plays a vital role in a IT developer's life.**GIT** is a Version Control software that makes collaboration super simple.  
>Git is a [_free and open source_](https://git-scm.com/about/free-and-open-source) distributed version control
 system designed to handle everything from small to very large projects with speed and efficiency.  
 >
 >Originally developed in 2005 by **Linus Torvalds**, _the famous creator of the Linux operating system kernel_.

 On git basically everything is done in Three steps.  
 * Working Tree(Modified)
 * Staging
 * Commit(Adding to git)

 ###Working Tree:  
 The _Working Tree_, or _Working directory_, consists of files that you are currently **working** on.  
 We can think of a working tree as a file system where you can view and modify files.
 The working area files are not handled by git. These files are also called as **Untracked files**. 

 ###Staging:  
 _Staging area_ is files that are going to be part of the next commit.

 ###Commit:
 The _commit_ is used to save your changes to the local repository.

 ##Few helpful commmands of git:  
 **git -log:** Helps to check logs and commit messages.  
 **git commit -a:** Stages files automatically.  
 **git show:** Shows various objects.  
 **git add:** To add a file to the Staging area.  
 **git commit:** Commits the file to the local repository.  
 **git checkout:** Its is effectively used to switch branches.  
 **git reset:** Basically resets the repo.  
 **git revert:** Makes a new commit which effectively rolls back a previous commit.    
 **git branch:** Used to manage branches.  
 **git branch name:** Creates a branch.  
 **git merge:** Merge joins the branches together.  
 **git clone URL:** Used to clone a remote repository into a local workspace.  
 **git push:** Used to push commits from local repository to a remote repo.  
 **git pull:** Used t0 fetch the newest updates from the remote repo.  
 **git remote updates:** Fetches the most up-to-date objects.  
 **git fetch:** Similar to pull but preview type.  
 **git rebase:** Can help to make the commit history linear.

 
